//
//  main.c
//  CommunicationArduino
//
//  Created by Megha Kalia, grad2021-msl-v on 11/16/21.
//  Copyright � 2021 Megha Kalia, grad2021-msl-v. All rights reserved.
//

/* www.chrisheydrick.com

 June 23 2012
 CanonicalArduinoRead write a byte to an Arduino, and then
 receives a serially transmitted string in response.
 The call/response Arduino sketch is here:
 https://gist.github.com/2980344
 Arduino sketch details at www.chrisheydrick.com
 */


#include "SimpleSerial.h"
//#include <unistd.h>
//#include <Windows.h>

#define DEBUG 1
using namespace std;

#define BUFSIZE 1025



int main(int argc, const char* argv[]) {
   
    char buf[BUFSIZE] = { '\0' };

    char com_port[] = "\\\\.\\COM3";
    DWORD COM_BAUD_RATE = CBR_9600;
    SimpleSerial Serial(com_port, COM_BAUD_RATE);

    if (Serial.connected_) {
        std::cout << "connection established \n" << endl; 
    }

    std::this_thread::sleep_for(std::chrono::microseconds(3500000)); 

    //write handshake probably 
    //char to_send[] = {'!'};
    string read_in = "1,2,3,4";

    char* to_send = &read_in[0];
    bool is_sent = Serial.WriteSerialPort(to_send);

    if (is_sent) {
        std::cout << "send data established \n" << endl;
    }
   
    //handshake 
    char handshake_char[] = { '!' };
    bool is_sent_handshake = Serial.WriteSerialPort(handshake_char);

    //read 
    if (Serial.ReadSerialPortMegha(buf, strlen("Set") + 2))
    {
        std::cout << buf << endl;
    }

    return 0;
}
